﻿using System;
using System.Net.Http;
using HttpVideoStreamerLib.Sender;
using HttpVideoStreamerLib.Receiver;

namespace HttpVideoStreamerLib.Test
{
    class Program
    {
		const string RECEIVING_POINT = "http://localhost:9981/";


		static int Main(string[] args)
		{
			Utils.OnLogging += (s, e) => Console.Error.WriteLine(s.ToString() + " " + e);

			IVideoReceiverController receiver = VideoSessionReceiverHandler.ListenTo(RECEIVING_POINT);

			Console.WriteLine(0);

			receiver.OnVideoRequestReceived += async delegate(object s, Uri videoSource)
			{				
				Console.WriteLine(videoSource.ToString());
                HttpClient client = new HttpClient();
                await client.GetStreamAsync(videoSource);
                client.Dispose();
			};
			receiver.OnVideoStatusChangeRequest += (s, o) => Console.WriteLine(o);

			Console.WriteLine(1);

			IVideoSenderController sender = VideoSessionSenderHandler.ConnectAsync(new Uri(RECEIVING_POINT), "../../../../VideoExamples/HitlerITT.mp4").Result;

			Console.WriteLine(2);

			sender.Play().Wait();

			Console.WriteLine(3);

			return (int)sender.GetHttpStatusResult();
        }
    }
}
