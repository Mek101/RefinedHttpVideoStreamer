﻿using System;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HttpVideoStreamerLib
{
    /// <summary>
    /// Implementations objects are notified when a context they're subscribed to arrives. 
    /// </summary>
    internal interface IHttpListenerNotifable : IDisposable
    {
		/// <summary>
		/// Raise to remove the current object from the subscribed <see cref="HttpListenerScheduler"/>.
		/// </summary>
        event Func<IHttpListenerNotifable, bool> OnUnsubscribeRequest;

		/// <summary>
		/// Called when a context about the relative prefix is received.
		/// May be raised asyncronously.
		/// </summary>
		/// <param name="context"></param>
        void ContextReceived(HttpListenerContext context);

		/// <summary>
		/// The prefixes the object is listening to.
		/// </summary>
        IEnumerable<string> Prefixes { get; }
    }
}
