﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace HttpVideoStreamerLib.Sender
{
	public static class VideoSessionSenderHandler
    {
		#region Fields and proprieties
		private static readonly HttpClient _client;

        public static Uri LocalPrefix { get { return _client.BaseAddress; } }
        #endregion


        #region Constructors
        /// <summary>
        /// Represents a video streaming session to a given number of receivers. 
        /// </summary>
        /// <param name="localPrefix">The uri prefix of this machine.</param>
        static VideoSessionSenderHandler() { _client = new HttpClient(); }
        #endregion


        #region Connectors
		/// <summary>
		/// Connects to a remote receiver.
		/// </summary>
		/// <param name="destination">The receiver.</param>
		/// <param name="videoPath">Path to the video to diplay.</param>
		/// <returns>A video stream controller.</returns>
		public static async Task<IVideoSenderController> ConnectAsync(Uri destination, string videoPath)
		{
			#region Checks
			// Checking destination uri validity.
			if(destination == null)
				throw new ArgumentNullException(nameof(destination), "Destination Uri can not be null.");
			if(!destination.IsWellFormedOriginalString())
				throw new ArgumentException("Unescaped Uri '" + destination.ToString() + "'", nameof(destination));

			// Cheking path validity.
			foreach(char c in Path.GetInvalidPathChars())
				if(videoPath.Contains(c))
					throw new ArgumentException("Unsupported character in file name or path '" + c + "'.", nameof(videoPath));

			foreach(char c in Path.GetInvalidFileNameChars())
				if(Path.GetFileName(videoPath).Contains(c))
					throw new ArgumentException("Unsupported character in file name '" + c + "'.", nameof(videoPath));
			#endregion

			Utils.Log(typeof(VideoSessionSenderHandler), "sending command.");


			// Generating a key corresponding to the video.
			string videoToken = (videoPath.GetHashCode() * destination.GetHashCode() / 3).ToString();

            // Prepares the controlled in advance.
            VideoSenderController controller = new VideoSenderController(_client, destination, videoToken, videoPath);
            // Binds the controller to the scheduler.
            HttpListenerScheduler.Subscribe(controller);

            // Contacts the receiver.
            HttpResponseMessage response = await _client.PostAsync(destination , new StringContent(videoToken));

            // If the receiver response was accepted.
            if(response.StatusCode == HttpStatusCode.Accepted)            
                return controller;            
            else
            {
                // Disposes the now useless controller;
                controller.Dispose();
                Utils.Log(typeof(VideoSessionSenderHandler), "Unattended response: ", response.StatusCode.ToString());
                return new VideoSenderFailed(response.StatusCode);
            }
        }
		#endregion
	}
}
