﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace HttpVideoStreamerLib.Sender
{
	internal class VideoSenderController : IVideoSenderController, IHttpListenerNotifable
    {
		#region Fields and prorieties
		// Remote host information
		readonly HttpClient _client;
		readonly Uri _remoteReceiver;

        const int BUFFER_SIZE = 4000;
        readonly string _videoPath;
		readonly string _videoToken;
        HttpVideoStatus _videoStatus;
		private HttpStatusCode _httpStatusResult;

		// Thread stuff
		private Thread _operation;
		private CancellationTokenSource _tokenSource;
		private readonly CancellationToken _token;
		private readonly object _videoStatusLock;
		private readonly ManualResetEvent _operationEndLock;

		// Public proprieties
		public bool IsPlaying => _operation.ThreadState == ThreadState.Running;
        public ThreadState BufferingStatus => _operation.ThreadState;
        public HttpVideoStatus VideoStatus
        {
			get { lock(_videoStatusLock) return _videoStatus; }
			private set
			{
				Monitor.Enter(_videoStatusLock);
				if(_videoStatus != value)
				{
					_videoStatus = value;

					Monitor.Exit(_videoStatusLock);

                    //Raises the event when the status changes
                    RaiseOnStreamStatusChanged(value);
				}
				else
					Monitor.Exit(_videoStatusLock);
			}
		}

		// Prefix of the receiver for the HttpListenerScheduler
		public IEnumerable<string> Prefixes => new string[] { _remoteReceiver.ToString() };
		#endregion


		/// <summary>
		/// Occurs when the status of the streaming has changed.
		/// </summary>
		public event EventHandler<HttpVideoStatus> OnStreamStatusChanged;


        /// <summary>
        /// IHttpListenerNotifable implementation.
        /// Requesting to unsubscribe the sender class.
        /// </summary>
        public event Func<IHttpListenerNotifable, bool> OnUnsubscribeRequest;


        #region Construct and memory management
        public VideoSenderController(HttpClient client, Uri remoteReceiver, string videoToken, string videoPath)
        {
            _client = client;
			_remoteReceiver = remoteReceiver;
			_videoToken = videoToken;
            _videoPath = videoPath;

			_operation = new Thread(SendVideo);
			_operationEndLock = new ManualResetEvent(false);
			_tokenSource = new CancellationTokenSource();
            _token = _tokenSource.Token;

			_videoStatusLock = new object();

			VideoStatus = HttpVideoStatus.Waiting;
		}

        ~VideoSenderController() => Dispose();

        public void Dispose()
        {
			if(_tokenSource != null)
			{
                _tokenSource.Cancel();
				_tokenSource.Dispose();
				_tokenSource = null;
			}

			if(OnStreamStatusChanged != null)
			{
				VideoStatus = HttpVideoStatus.Stopped;                    
				OnStreamStatusChanged = null;
			}

			if(_operation != null)
			{
				if(_operation.ThreadState == ThreadState.Running)
					_operation.Abort();

				_operation = null;
			}

            RaiseOnUnsubscribeRequest();
        }
        #endregion


        #region Stream control
        /// <summary>
        /// Starts the streaming.
        /// </summary>
        public async Task<HttpStatusCode> Play()
        {
            HttpResponseMessage response = await _client.PostAsync(_remoteReceiver, new StringContent(HttpVideoMethods.Play));
            if(response.StatusCode == HttpStatusCode.OK)
                VideoStatus = HttpVideoStatus.Playing;
            return response.StatusCode;
        }


        /// <summary>
        /// Blocks the streaming.
        /// </summary>
        public async Task<HttpStatusCode> Pause()
        {
            HttpResponseMessage response = await _client.PostAsync(_remoteReceiver, new StringContent(HttpVideoMethods.Pause));
            if(response.StatusCode == HttpStatusCode.OK)
                VideoStatus = HttpVideoStatus.Paused;
            return response.StatusCode;
        }


        /// <summary>
        /// Aborts the streaming.
        /// </summary>
        public async Task<HttpStatusCode> Stop()
        {
            if(!_token.IsCancellationRequested)
                _tokenSource.Cancel();

            HttpResponseMessage response = await _client.PostAsync(_remoteReceiver, new StringContent(HttpVideoMethods.Stop));
            if(response.StatusCode == HttpStatusCode.OK)
                VideoStatus = HttpVideoStatus.Stopped;
            return response.StatusCode;
        }


        private void RaiseOnStreamStatusChanged(HttpVideoStatus status)
        {
            if(OnStreamStatusChanged != null)
                OnStreamStatusChanged(this, status);

            if(status == HttpVideoStatus.Stopped)
                RaiseOnUnsubscribeRequest();
        }

        private void RaiseOnUnsubscribeRequest()
        {
            if(OnUnsubscribeRequest != null)
                OnUnsubscribeRequest(this);
        }
		#endregion


		#region Joiners
		/// <summary>
		/// Blocks the calling thread until the streaming finishes.
		/// </summary>
		public void WaitEndStream() => _operationEndLock.WaitOne();


		public WaitHandle WaitHandle() => _operationEndLock;


		/// <summary>
		/// Blocks the calling thread until the operations finishes, then returns the result.
		/// </summary>
		/// <returns>The status returned at the end of the video streaming.</returns>
		public HttpStatusCode GetHttpStatusResult()
		{
			_operationEndLock.WaitOne();
			return _httpStatusResult;
		}
		#endregion


		/// <summary>
		/// IHttpListenerNotifable implementation.
		/// Raised by the HttpListenerScheduler when a context from a prefix is received.
		/// </summary>
		/// <param name="context">Received context.</param>
		public void ContextReceived(HttpListenerContext context)
		{
			Utils.Log(this, "new context received: " + context.Request.HttpMethod + " from " + context.Request.RemoteEndPoint + ".");

			// Switch doesn't seem to support proprieties
			if(context.Request.HttpMethod.Equals(HttpMethod.Get.Method))
				// The receiver request for the video. Starts the thread.
				_operation.Start(context);
			else if(context.Request.HttpMethod.Equals(HttpMethod.Delete.Method))
			{
				// The video reproduction was terminated by the receiver.
				VideoStatus = HttpVideoStatus.Stopped;
				context.Response.StatusCode = (int)HttpStatusCode.OK;
				context.Response.Close();
			}
			else
			{
				context.Response.StatusCode = (int)HttpStatusCode.NotImplemented;
				context.Response.StatusDescription = "Unknow or not implemented method.";
				context.Response.Close();
			}
		}


		/// <summary>
		/// The sending operation.
		/// </summary>
		/// <returns>The status code returned by the receiver at the end of the streaming.</returns>
		private void SendVideo(object container)
		{
			// Unboxing...
			HttpListenerContext context = (HttpListenerContext)container;

			// Code and status description to return 
			HttpStatusCode statusCode = HttpStatusCode.InternalServerError; // If it returns this, something __really__ bad happened.
			string statusDescription = string.Empty;

			// Extracts the request code.
			string requestContentToken = Utils.GetContent(context.Request.InputStream, context.Request.ContentEncoding);

			// Tests the validity of the request token..
			if(_videoToken.Equals(requestContentToken))
			{
				// Loads file to the buffer response.
				using(FileStream fileStream = new FileStream(_videoPath, FileMode.Open, FileAccess.Read, FileShare.Read))
				{
					byte[] buffer = new byte[BUFFER_SIZE];
					context.Response.ContentLength64 = fileStream.Length;

					// Breaks the cycle if the operation is canceled or the data lentgh of the streams is still different
					while(true)
					{
						if(_token.IsCancellationRequested)
						{
							statusCode = HttpStatusCode.Gone;
							statusDescription = "Request canceled.";
							break;
						}

						// Copies 4KB of data to the buffer
						int read = fileStream.Read(buffer, 0, BUFFER_SIZE);

						if(read == 0)
						{
							// Successse
							statusCode = HttpStatusCode.OK;
							break;
						}

						context.Response.OutputStream.Write(buffer, 0, read);

						Utils.Log(this, "sent " + read + " bytes out of " + fileStream.Length + ".");
					}
				}
			}
			else
			{
				// Invalid request
				statusCode = HttpStatusCode.Forbidden;
				statusDescription = "Bad token '" + requestContentToken + "'.";
			}


			// Closes the context and sends the response.
			context.Response.StatusCode = (int)statusCode;
			context.Response.StatusDescription = statusDescription;
			context.Response.Close();

			_httpStatusResult = statusCode;

			_operationEndLock.Set();
		}
    }



	/// <summary>
	/// A lighter version of VideoStreamController class, provviding a valid object in case of failure.
	/// </summary>
	internal class VideoSenderFailed : IVideoSenderController
    {
        private readonly HttpStatusCode _code;
        private readonly Task<HttpStatusCode> _dummyTask;

		public bool IsPlaying => false;
		public ThreadState BufferingStatus => ThreadState.Unstarted;
		public HttpVideoStatus VideoStatus => HttpVideoStatus.Stopped;

        public VideoSenderFailed(HttpStatusCode code)
        {
            _code = code;
            _dummyTask = Task.FromResult<HttpStatusCode>(code);
        }

        public Task<HttpStatusCode> Play()  => _dummyTask;
        public Task<HttpStatusCode> Pause() => _dummyTask;
        public Task<HttpStatusCode> Stop()  => _dummyTask;

        public void WaitEndStream() { }

        public HttpStatusCode GetHttpStatusResult() => _code;

        public void Dispose() { }            
    }
}
