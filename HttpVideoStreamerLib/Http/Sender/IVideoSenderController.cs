﻿using System;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace HttpVideoStreamerLib.Sender
{
    public interface IVideoSenderController : IDisposable
    {
        bool IsPlaying { get; }
        ThreadState BufferingStatus { get; }
        HttpVideoStatus VideoStatus { get; }

        Task<HttpStatusCode> Play();
        Task<HttpStatusCode> Pause();
        Task<HttpStatusCode> Stop();

        void WaitEndStream();

		HttpStatusCode GetHttpStatusResult();
    }
}
