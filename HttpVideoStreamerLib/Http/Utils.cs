﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HttpVideoStreamerLib
{
	public static class Utils
    {
        public static event EventHandler<string> OnLogging;

        public static void Log(object sender, params string[] text)
        {
            if(OnLogging != null && text != null && text.Length > 0)
                OnLogging(sender, string.Concat(text));
        }


		/// <summary>
		/// Extracts the contents from a stream to string with the given encoding.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="encoding"></param>
		/// <returns></returns>
		internal static string GetContent(Stream stream, Encoding encoding)
		{
			List<byte> buffer = new List<byte>(48);
			byte[] tempBuffer = new byte[48];
			int read = 0;

			while(true)
			{
				read = stream.Read(tempBuffer, 0, tempBuffer.Length);

				if(read == tempBuffer.Length)
					buffer.AddRange(tempBuffer);
				else
				{
					for(int i = 0; i < read; i++)
						buffer.Add(tempBuffer[i]);

					break;
				}
			}

			return encoding.GetString(buffer.ToArray());
		}
	}
}
