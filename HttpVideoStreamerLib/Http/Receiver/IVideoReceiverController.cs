﻿using System;
using System.Collections.Generic;

namespace HttpVideoStreamerLib.Receiver
{
	public interface IVideoReceiverController : IDisposable
	{
		/// <summary>
		/// The prefixes the controller is listening to for Post requests.
		/// </summary>
		IEnumerable<string> Prefixes { get; }

		/// <summary>
		/// A request to downlaod and reproduce the video at the given uri has been deliverled.
		/// </summary>
		event EventHandler<Uri> OnVideoRequestReceived;

		/// <summary>
		/// Raised when the sender requests to change the reproduction status of the video.
		/// The given string matches with one of the HttpVideoMethods static class.
		/// </summary>
		event EventHandler<string> OnVideoStatusChangeRequest;
	}
}
