﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;

namespace HttpVideoStreamerLib.Receiver
{
	/// <summary>
	/// Nested class.
	/// </summary>
	internal class VideoReceiverController : IVideoReceiverController, IHttpListenerNotifable
	{
		#region Fields
		public IEnumerable<string> Prefixes { get; private set; }

		public event Func<IHttpListenerNotifable, bool> OnUnsubscribeRequest;
		public event EventHandler<Uri> OnVideoRequestReceived;
		public event EventHandler<string> OnVideoStatusChangeRequest;
		#endregion

		#region Event Raisers
		private void RaiseUnsubscribeRequest()
		{
			if(OnUnsubscribeRequest != null)
				OnUnsubscribeRequest(this);
		}

		private void RaiseOnVideoRequestReceived(HttpListenerRequest sender, Uri videoSource)
		{
			if(OnVideoRequestReceived != null)
				OnVideoRequestReceived.BeginInvoke(sender, videoSource, (ar) => OnVideoRequestReceived.EndInvoke(ar), null);
		}

		private void RaiseOnVideoStatusChangeRequest(HttpListenerRequest sender, string method)
		{
			if(OnVideoStatusChangeRequest != null)
				OnVideoStatusChangeRequest.BeginInvoke(sender, method, (ar) => OnVideoStatusChangeRequest.EndInvoke(ar), null);
		}
		#endregion


		#region Constructors and resource management
		/// <summary>
		/// Instantiates a new <see cref=VideoReceiverController"/> object.
		/// </summary>
		/// <param name="prefixes">The prefixes to listen to.</param>
		public VideoReceiverController(IEnumerable<string> prefixes)
		{
			Prefixes = prefixes;
		}

		~VideoReceiverController() => Dispose();

		public void Dispose()
		{
			RaiseUnsubscribeRequest();
			OnUnsubscribeRequest = null;
			OnVideoRequestReceived = null;
			Prefixes = null;
		}
		#endregion


		#region Post request handling
		public void ContextReceived(HttpListenerContext context)
		{
			Utils.Log(this, "Post method: ", HttpMethod.Post.Method);

			// Checks the request method
			if(context.Request.HttpMethod == HttpMethod.Post.Method)
			{
				try
				{
					string content = Utils.GetContent(context.Request.InputStream, context.Request.ContentEncoding);

					// Filtrates the given command and raises the appropriate event.
					if(content == HttpVideoMethods.Play || content == HttpVideoMethods.Pause || content == HttpVideoMethods.Stop)
						HandleMethodPost(context, content);
					else
						HandleVideoPost(context, content);
				}
				catch(Exception e)
				{
                    try
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        context.Response.StatusDescription = "An exception was thrown inside the server. Please, contact an administrator.";
                        context.Response.Close();
                    }
                    catch(Exception) { }

					Utils.Log(this, e.ToString());
					throw e;
				}
			}
			// Invalid request
			else
			{
				Utils.Log(this, "Expecting a POST method from " + context.Request.Url + ", recived '" + context.Request.HttpMethod + "' instead");
				context.Response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
				context.Response.StatusDescription = "Processing only file post.";

				// Closes the request, sending the response
				context.Response.Close();
			}
		}


		private void HandleMethodPost(HttpListenerContext context, string method)
		{
			RaiseOnVideoStatusChangeRequest(context.Request, method);
			context.Response.StatusCode = (int)HttpStatusCode.OK;
			context.Response.Close();
		}


		private void HandleVideoPost(HttpListenerContext context, string videoSource)
		{
			// Extracts the video uri to download and play.				
			Utils.Log(this, "Post content: ", videoSource);
			// Contacts the origin
			UriBuilder builder = new UriBuilder(context.Request.Url);
			builder.Path = videoSource;
			Uri remoteVideo = builder.Uri;


			// Checks the uri integrity and validity
			if(remoteVideo.IsWellFormedOriginalString())
			{
				// Checks if the video is a file on the local machine
				if(remoteVideo.IsFile)
				{
					Utils.Log(this, "Post url is a local file: ", context.Request.Url.AbsolutePath, "\nQuery: ", context.Request.Url.Query);
					context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
					context.Response.StatusDescription = "Url is a local file.";
				}
				else
				{
					// Success
					context.Response.StatusCode = (int)HttpStatusCode.Accepted;

					// Raises the event						
					RaiseOnVideoRequestReceived(context.Request, remoteVideo);
				}
			}
			// Invalid url
			else
			{
				Utils.Log(this, "Bad format: '" + remoteVideo.ToString() + "'.");
				context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
				context.Response.StatusDescription = "Invalid url.";
			}

			// Closes the request, sending the response
			context.Response.Close();
		}
		#endregion
	}
}