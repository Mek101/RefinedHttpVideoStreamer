﻿using System.Collections.Generic;

namespace HttpVideoStreamerLib.Receiver
{
	public static class VideoSessionReceiverHandler
	{
		public static IVideoReceiverController ListenTo(string prefix) => ListenTo(new string[] { prefix });

		/// <summary>
		/// Creates a new <see cref="IVideoReceiverController"/> receiver over the given prefixes.
		/// </summary>
		/// <param name="prefixes">The prefixes the receiver listens to.</param>
		/// <returns>A new <see cref="IVideoReceiverController"/> instance.</returns>
		public static IVideoReceiverController ListenTo(IEnumerable<string> prefixes)
		{
			VideoReceiverController receiver = new VideoReceiverController(prefixes);
			HttpListenerScheduler.Subscribe(receiver);
			return receiver;
		}
	}
}
