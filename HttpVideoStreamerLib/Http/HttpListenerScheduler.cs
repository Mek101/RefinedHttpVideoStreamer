﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using TupleContainer = System.ValueTuple<System.Net.HttpListener, string>;

namespace HttpVideoStreamerLib
{
	/// <summary>
	/// Notifies the Binded objects when HttpListenerContext arravies from the prefixes they're listening to.
	/// </summary>
	internal static class HttpListenerScheduler
    {
		// Subscribed objects.
		private static readonly List<IHttpListenerNotifable> _subscribers;
		// Prefixes with correlated listeners.
		private static readonly Dictionary<string, HttpListener> _listeners;


		#region Construcotrs and memory management
		/// <summary>
		/// Initializes a new instance of the <see cref="T:HttpVideoStreamer.Http.HttpListenerScheduler"/> class.
		/// </summary>
        static HttpListenerScheduler()
        {
            _subscribers = new List<IHttpListenerNotifable>();
            _listeners = new Dictionary<string, HttpListener>();
        }


		/// <summary>
		/// Deletes all the pending operations and subscribers.
		/// </summary>
		public static void Clear()
		{
			foreach(HttpListener listener in _listeners.Values)
				listener.Abort();

			_listeners.Clear();
			_subscribers.Clear();
		}
		#endregion


		#region Binding
		/// <summary>
		/// Subscribes the given object to the listeners.
		/// </summary>
		/// <param name="subscriber"></param>
		public static void Subscribe(IHttpListenerNotifable subscriber)
        {
			if(!subscriber.Prefixes.Any())
				throw new ArgumentException("No prefixes to listen to");

            lock (_subscribers)
            {
                _subscribers.Add(subscriber);
                // Adds "automatic" unsubscription.
                subscriber.OnUnsubscribeRequest += Unsubscribe;

                foreach(string prefix in subscriber.Prefixes)
                {
                    if(!_listeners.ContainsKey(prefix))
                    {
                        HttpListener listener = new HttpListener();

                        // Starts a new task listening on the given prefix
                        Task.Factory.StartNew(ListenToPrefix, new TupleContainer(listener, prefix), TaskCreationOptions.LongRunning);
                        _listeners.Add(prefix, listener);
                    }
                }
            }
        }


        /// <summary>
        /// Unsubscribes the given object from the listeners.
        /// </summary>
        /// <param name="subscriber"></param>
        public static bool Unsubscribe(IHttpListenerNotifable subscriber)
        {
	        lock(_subscribers)
	        {
				if(_subscribers.Remove(subscriber))
				{
					foreach(string prefix in subscriber.Prefixes)
					{
                        // If there aren't other subscribers to the listened prefix
                        bool none = !_subscribers.Any((sub) => sub.Prefixes.Contains(prefix));

						// Cancels the listener task
						if(none)
						{
							HttpListener listener = _listeners[prefix];
							if(listener.IsListening)
							{
								listener.Stop();
								listener.Close();
							}
						}
					}

					return true;
				}
				else
					return false;
			}
        }
		#endregion


		/// <summary>
		/// Waits for contexts for the given prefixes.
		/// </summary>
		/// <param name="container">Boxed tuple with listener and prefix.</param>
		private static void ListenToPrefix(object container)
        {
			TupleContainer tuple = (TupleContainer)container;
			HttpListener listener = tuple.Item1;
            string prefix = tuple.Item2;

			listener.Prefixes.Add(prefix);
			listener.Start();

            try
            {
                while(true)
                {
                    Task.Yield();

                    HttpListenerContext context = listener.GetContext();

					// Notifies the subscribers to the current prefix.
					lock(_subscribers)
					{
						foreach(IHttpListenerNotifable subscriber in _subscribers)
							if(subscriber.Prefixes.Contains(prefix))
							{
								Task.Factory.StartNew(delegate()
								{
									subscriber.ContextReceived(context);
								});
							}
					}
                }                
            }
            catch(HttpListenerException e)
            {
				Utils.Log(typeof(HttpListenerScheduler), e.ToString());
            }
			finally
			{
				// Closes the listener.
				if(listener.IsListening)
					listener.Close();
			}
		}
	}
}
