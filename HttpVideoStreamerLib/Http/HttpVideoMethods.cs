﻿namespace HttpVideoStreamerLib
{
    public enum HttpVideoStatus
    {
	    Waiting, // Still waiting the receiver GET request
	    Playing,
	    Paused,
	    Stopped // Reproduction aborted
    };

    public static class HttpVideoMethods
    {
        public static string Play { get { return "PLAY"; } }

        public static string Stop { get { return "STOP"; } }

        public static string Pause { get { return "PAUSE"; } }
    }
}
