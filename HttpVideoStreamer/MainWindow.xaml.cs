﻿using System;
using System.Net;
using System.Windows;
using Microsoft.Win32;
using HttpVideoStreamerLib;
using HttpVideoStreamerLib.Receiver;
using HttpVideoStreamerLib.Sender;

namespace HttpVideoStreamer
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
    {
		IVideoReceiverController _receiver;
		IVideoSenderController _videoController;

        // The uri pointing to the receiver 
        string _fileName;
        int _receiverPort;


        #region Constructors
        public MainWindow()
        {
            InitializeComponent();

            // Event binding.
            mdElem_videoBox.MediaFailed += (s, e) => Utils.Log(mdElem_videoBox, "VideoBox: media failed.", e.ErrorException.ToString());
            Utils.OnLogging += (s, e) => Log(e);

            // Ports used by each host
            _receiverPort = 8081;

            // Initializing prefixes. Each object will listen for connections on the given prefix
            UriBuilder ub1 = new UriBuilder(Uri.UriSchemeHttp, IPAddress.Loopback.ToString(), _receiverPort);
			_receiver = VideoSessionReceiverHandler.ListenTo(ub1.Uri.ToString());
			_receiver.OnVideoRequestReceived += HttpPostHandler;

            lbl_senderPrefix.Content = "Sender prefix:\thttp://localhost:8080/";

            this.Closed += (s, e) => Dispose();
        }

        ~MainWindow() => Dispose();

        public void Dispose()
        {
            if (_videoController != null)
            {
                _videoController.Dispose();
                _videoController = null;
            }                
        }
        #endregion


        #region Event handlers
        /// <summary>
        /// Handler for the video command received.
        /// </summary>
        /// <param name="context">The context of the http request.</param>
        /// <param name="uri">Video source.</param>
        private void HttpPostHandler(object context, Uri uri)
        {
            this.Dispatcher.InvokeAsync(delegate()
            {
                try
                {
                    mdElem_videoBox.Source = uri;
                    mdElem_videoBox.Play();
                }
                catch(Exception e)
                {
                    Utils.Log(context, e.ToString(), "\n\nUri: ", uri.ToString());
                }
            });
        }
        #endregion


        #region Utils methods
        private void InterfaceLog(params object[] objects) => Log("Interface: ", string.Concat(objects));

        private void Log(params object[] objects)
        {
            string str = string.Empty;

            foreach(object o in objects) { str += o.ToString(); }

            this.Dispatcher.InvokeAsync(() => lstBox_logger.Items.Add(str));
        }
        #endregion


        #region Private methods
        private void LoadFile()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if((bool)dialog.ShowDialog())
            {
                _fileName = dialog.FileName;
                InterfaceLog("File selected.");
            }
            else
                InterfaceLog("Opening aborted.");
        }


        private async void Send()
        {
            if(_fileName == string.Empty)
                InterfaceLog("No file selected.");
            else
            {
                string ip = string.Empty;

                this.Dispatcher.Invoke(delegate()
                {
                    // Disables the button, to prevent interrupting the stream
                    btn_send.IsEnabled = false;
                    ip = txtBox_receiverIP.Text;
                });

                InterfaceLog("Uploading file...");
                try
                {
					_videoController = await VideoSessionSenderHandler.ConnectAsync(new Uri(Uri.UriSchemeHttp + "://" + ip + ":" + _receiverPort.ToString() + "/", UriKind.Absolute), _fileName.Remove(0, 2));

                    InterfaceLog("File uploading result: \"" + _videoController.VideoStatus.ToString() + "\"");
                }
                catch(Exception e) { InterfaceLog("ERROR: ", e.ToString()); }
                finally
                {
#pragma warning disable CS4014 // Non è possibile attendere la chiamata, pertanto l'esecuzione del metodo corrente continuerà prima del completamento della chiamata
                    this.Dispatcher.InvokeAsync(delegate()
                    {
                        // Ensures to re-enable the button
                        btn_send.IsEnabled = true;
                    });
#pragma warning restore CS4014
                }
            }
        }
        #endregion


        #region Buttons
        private void Btn_load_Click(object sender, RoutedEventArgs e)
        {
            Action a = LoadFile;
            a.BeginInvoke((ar) => a.EndInvoke(ar), null);
        }
        private void Btn_send_Click(object sender, RoutedEventArgs e)
        {
            Action a = Send;
            a.BeginInvoke((ar) => a.EndInvoke(ar), null);
        }


        private void Btn_play_Click(object sender, RoutedEventArgs e)
        {
            if(_videoController != null && !_videoController.IsPlaying)
                _videoController.Play();
        }

        private void Btn_pause_Click(object sender, RoutedEventArgs e)
        {
            if(_videoController != null && _videoController.IsPlaying)
                _videoController.Pause();
        }

        private void Btn_stop_Click(object sender, RoutedEventArgs e)
        {
            if(_videoController != null)
                _videoController.Stop();
        }
        #endregion
    }
}